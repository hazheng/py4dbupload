#!/usr/bin/env python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 08-August-2020

# This script masters the registration of the Halfmoon components.

import os,traceback,sys

from AnsiColor  import Fore, Back, Style
from DataReader import TableReader
from Sensor     import *
from Utils      import *
from optparse   import OptionParser


if __name__ == "__main__":
   p = OptionParser(usage="usage: %prog [options] <serial number file>, ...", version="1.1")

   p.add_option( '-d','--data',
               type    = 'string',
               default = '',
               dest    = 'data_path',
               metavar = 'STR',
               help    = 'Path to the excel files with measurement data.')

   p.add_option( '--date',
               type    = 'string',
               default = '2020-08-01',
               dest    = 'date',
               metavar = 'STR',
               help    = 'Date of the component registration')

   p.add_option( '-o','--operator',
               type    = 'string',
               default = '',
               dest    = 'operator',
               metavar = 'STR',
               help    = 'The operator that performed the QC measurement')

   p.add_option( '--verbose',
               action  = 'store_true',
               default = False,
               dest    = 'verbose',
               help    = 'Force the uploaders to print their configuration and data')

   (opt, args) = p.parse_args()

   if len(args)<1:
      p.error('need at least 1 argument!')


   if opt.verbose:  BaseUploader.verbose = True

   db = DBaccess(verbose=opt.verbose)

   # write the configuration dictionary for the Halfmoon components
   halfmoon_conf = {
      'product_date'     : opt.date,
      'manufacturer'     : 'Hamamatsu',
      'unique_location'  : 'Hamamatsu',
      'description'      : 'Halfmoon from Pre Production.',
      'attributes'       : [('Sensor Type','PreProduction'),('Status','Good')],
   }

   for halfmoon_sn_file in args:
      listname = os.path.splitext(os.path.basename(halfmoon_sn_file))[0]
      hm_list = TableReader(halfmoon_sn_file)
      hm = HalfmoonFromIDlist(listname,halfmoon_conf,db,hm_list)
      hm.dump_xml_data()
      print (hm_list)
