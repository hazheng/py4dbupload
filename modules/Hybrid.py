#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 13-Nov-2019

# This module contains the classes that produce the XML file for uploading
# the Hybrid components.

from BaseUploader import BaseUploader, ConditionData
from lxml         import etree
from shutil       import copyfile
from copy         import deepcopy
from Exceptions   import *
from Utils        import search_files
from enum         import Enum
from datetime     import datetime

import json


HybridTest = Enum('HybridTest','Electrical, Visual')
ReferenceT = Enum('ReferenceT','POH_acc, FEH_acc, SEH_acc, ROH_acc')

class FEHybrid(BaseUploader):
   """Class to produce the xml file for the DBloader upload. It handles the
      FE Hybrid component data."""

   def __init__(self, name , cdict):
      """Constructor: it requires the instance name, the dict for configuration
         (cdict) defining the ancillary data needed for the database upload. 
         Mandatory parameters in configuration:
            manufacturer:      the manufacturer of the component;
            kind_of_part:      the part to be inserted in the database;
            version:           the version of 8CBC3 hybrid produced;
            unique_location:   the location where all the components are;
      """
      BaseUploader.__init__(self, name, cdict)

      self.mandatory += ['manufacturer','kind_of_part','version',\
                         'unique_location']

   def dump_xml_data(self, filename='', uploaded=[], excluded=[]):
      """Writes the wafer data in the XML file for the database upload.
         Optional input parameters:
           filename    name of the xml file to be produced;
           excluded    list of serial IDs to be excluded from the upload; 
         Output parameter: 
           uploaded    list of serial id written in the file."""
      
      
      inserter  = self.retrieve('inserter')
      root_node = 'ROOT' if inserter is None else f'ROOT operator_name="{inserter}"'
      root      = etree.Element(root_node)
      part_tree = etree.SubElement(root,"PARTS")

      self.xml_builder(part_tree,uploaded,excluded)

      if filename == '':
         filename = '%s_FEHybrids.xml'%self.name
      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                        xml_declaration = True,\
                                        standalone="yes").decode('UTF-8') )

      return filename



class Upload8CBC3(FEHybrid):
   """Produces the XML file to register or to update the 8CBC3 FE Hybrid components.""" 

   allowed_values = { 
                      'status' : ['Good','Bad'],
                      'chip_type' : ['CBC3.0','CBC3.1'],
                      'sensor_spacing' : ['1.8 mm'],
                      'stiffener_type' : ['FR4','CF-K13D2U'],
                      'assembly_state' : ['Fully assembled','Bare','Dummy hybrid'],
                      'manufacturer'   : ['Valtronic','AEMtec']
                    }

   def __init__(self, serial, cdict, status=None, chipType=None, sensorSpacing=None,\
                stiffenerType=None, assemblyState=None, comment=None):
      """Constructor: it requires the serial number of the FE Hybrid to upload and the
         configuration dictionary. Optional parameneters are:
           status             [Good,Bad]
           chip_type          the type of the chip [CBC3.0,CBC3.1];
           sensor_spacing     the sensor spacing [1.8 mm];
           stiffener_type     the stiffener type [FR4,CF-K13D2U]
           assembly_state     [Fully assembled,Bare,Dummy hybrid];
           comment            description of the component.
      """
      FEHybrid.__init__(self, serial, cdict)

      # Override the configuration dictionary with the input parameters
      if status!=None:        self.update_configuration('status',status)
      if chipType!=None:      self.update_configuration('chip_type',chipType)
      if sensorSpacing!=None: self.update_configuration('sensor_spacing',sensorSpacing)
      if stiffenerType!=None: self.update_configuration('stiffener_type',stiffenerType)
      if assemblyState!=None: self.update_configuration('assembly_state',assemblyState)
      if comment!=None:       self.update_configuration('description',comment)

      # Check attribute  and configuration values
      c_status = self.retrieve('status')
      if c_status!=None and c_status not in self.allowed_values['status']:
         raise NotRecognized('FEHybrid',c_status,'as Status attribute')

      c_chip = self.retrieve('chip_type')
      if c_chip!=None and c_chip not in self.allowed_values['chip_type']:
         raise NotRecognized('FEHybrid',c_chip,'as Chip type attribute')

      c_spacing = self.retrieve('sensor_spacing')
      if c_spacing!=None and c_spacing not in self.allowed_values['sensor_spacing']:
         raise NotRecognized('FEHybrid',c_spacing,'as Sensor spacing attribute')

      c_stiffener = self.retrieve('stiffener_type')
      if c_stiffener!=None and c_stiffener not in self.allowed_values['stiffener_type']:
         raise NotRecognized('FEHybrid',c_stiffener,'as Stiffener type attribute')

      c_assembly = self.retrieve('assembly_state')
      if c_assembly!=None and c_assembly not in self.allowed_values['assembly_state']:
         raise NotRecognized('FEHybrid',c_assembly,'as Assembly state attribute')

      c_manuf = self.retrieve('manufacturer')
      if c_manuf!=None and c_manuf not in self.allowed_values['manufacturer']:
         raise NotRecognized('FEHybrid',c_manuf,'as manufacturer')


      # Override version in configuration dictionary
      if c_chip!=None and c_spacing!=None and c_stiffener!=None:
         version_str = '{}, {}, {}'.format(c_chip,c_spacing,c_stiffener)
         self.update_configuration('version',version_str)



   def xml_builder(self,parts,*args):
      """Process data."""
      # Gather the list of components to be excluded
      excluded = args[1]

      # Gather the serial number of component
      Serial = self.name
      if Serial in excluded: return

      c_status    = self.retrieve('status')
      c_chip      = self.retrieve('chip_type') 
      c_spacing   = self.retrieve('sensor_spacing')
      c_stiffener = self.retrieve('stiffener_type')
      c_assembly  = self.retrieve('assembly_state')

      hybrid_attributes = []
      if c_status!=None:    hybrid_attributes.append( ('Status',c_status) )
      if c_chip!=None:      hybrid_attributes.append( ('Chip type',c_chip) )
      if c_spacing!=None:   hybrid_attributes.append( ('2S Sensor spacing',c_spacing) )
      if c_stiffener!=None: hybrid_attributes.append( ('Stiffener type',c_stiffener) )
      if c_assembly!=None:  hybrid_attributes.append( ('Assembly state',c_assembly) )
      if len(hybrid_attributes)!=0: 
         self.configuration['attributes']  = hybrid_attributes

      self.build_parts_on_xml(parts,serial=Serial)

      try:
         args[0].append( Serial )
      except:
         pass



class Hybrid():
   """Class to upload the Hybrid data into the database."""
   
   allowed_attributes = { 
             'Status'                   : ['Good','Bad'],
             'Visual inspection status' : ['Good' , 'Bad'],
             'Electrical test status'   : ['Good' , 'Bad'],
             'Assembly state'           : ['Fully assembled','Bare','Dummy hybrid'], # not attached
             'Stiffener type'           : ['FR4','CF-K13D2U'],                       # not attached
             'Chip type'                : ['CBC3.0','CBC3.1'],                       # not attached
             'FE Hybrid Side'           : ['Right','Left'],
             '2S Sensor spacing'        : ['1.8 mm','4.0 mm'],
             'PS Sensor spacing'        : ['1.6 mm','2.6 mm','4.0 mm'],
                     }
   
   def __init__(self, serial):
      """Constructor: it requires the serial number that is used to decode the hybrid properties."""
      try:
         self.serial = serial                  # stores the serial number
         self.h_type = serial.split('-')[0]    # stores the first section of the serial number
         self.h_kind = None                    # stores the kind_of_part
         self.h_tick = None                    # stores the spacing code
         self.h_side = None                    # stores the side code
         self.h_code = serial.split('-')[1]    # stores the last section of the serial number
         self.h_vend = None                    # stores the vendor name
         self.h_lot  = self.h_code[1:3]        # stores the batch number
      
         if self.h_code[0]=='2': self.h_vend = 'Valtronic'
         if self.h_code[0]=='3': self.h_vend = 'AEMtec'

         if '2SFEH' in self.h_type or '2S-FEH' in self.h_type: 
            self.h_kind = '2S Front-end Hybrid' 
            self.h_tick = self.h_type[-3:-1]
            self.h_side = self.h_type[-1] 

         if '2SSEH' in self.h_type or '2S-SEH' in self.h_type: 
            self.h_kind = '2S Service Hybrid' 
                                                                            
         if 'PSFEH' in self.h_type or 'PS-FEH' in self.h_type:  
            self.h_kind = 'PS Front-end Hybrid' 
            self.h_tick = self.h_type[-3:-1] 
            self.h_side = self.h_type[-1]

         if 'PSROH' in self.h_type or 'PS-ROH' in self.h_type: 
            self.h_kind = 'PS Read-out Hybrid' 
            self.h_tick = self.h_type[-3:-1] 

         if 'PSPOH' in self.h_type or 'PS-POH' in self.h_type: 
            self.h_kind = 'PS Power Hybrid' 

      except:
         raise NotRecognized(self.__class__.__name__,serial,\
                     'as serial number for any Hybrid type!')






class HybridComponentT(Hybrid,BaseUploader):
   """Produces the XML file to register any Hybrid components.""" 

   def __init__(self, serial, location, comment=None, inserter=None):
      """Constructor for component registration: it requires the serial number of the Hybrid and
         the hybrid location to produce the configuration dictionary. 
         Optional parameneters are:
           comment:      description of the component.
           inserter:     person recording the data (override the CERN user name)
      """
      Hybrid.__init__(self,serial)

      name = '{}_step0'.format(serial)
      cdict = {}
      self.attributes = []
      
      if self.h_kind == '2S Front-end Hybrid':         # Attributes not defined for 2S-FEH:
                                                       #   Visual inspection status [Good , Bad]
                                                       #   Electrical test status [Good , Bad]
                                                       #   CIC Version [CIC1 , CIC2]
         if self.h_tick=='18':  self.load_attribute('2S Sensor spacing','1.8 mm')
         if self.h_tick=='40':  self.load_attribute('2S Sensor spacing','4.0 mm')
         if self.h_side=='R':   self.load_attribute('FE Hybrid Side','Right')
         if self.h_side=='L':   self.load_attribute('FE Hybrid Side','Left')

      if self.h_kind == '2S Service Hybrid' :          # Attributes not defined for 2S-SEH:
         pass                                          #   Visual inspection status [Good , Bad]
                                                       #   Electrical test status [Good , Bad]
                                                       #   LPGBT Version [V0 , V1]
                                                      
      if self.h_kind == 'PS Front-end Hybrid':         # Attributes not defined for PS-FEH:
                                                       #   Visual inspection status [Good , Bad]
                                                       #   Electrical test status [Good , Bad]
                                                       #   SSA Version [SSA1 , SSA2]
                                                       #   CIC Version [CIC1 , CIC2]
         if self.h_tick=='16':  self.load_attribute('PS Sensor spacing','1.6 mm')
         if self.h_tick=='26':  self.load_attribute('PS Sensor spacing','2.6 mm')
         if self.h_tick=='40':  self.load_attribute('PS Sensor spacing','4.0 mm')
         if self.h_side=='R':   self.load_attribute('FE Hybrid Side','Right')
         if self.h_side=='L':   self.load_attribute('FE Hybrid Side','Left')

      if self.h_kind == 'PS Read-out Hybrid':          # Attributes not defined for PS-ROH:
                                                       #   Visual inspection status [Good , Bad]
                                                       #   Electrical test status [Good , Bad]
                                                       #   LPGBT Version [V0 , V1]
                                                       #   LpGBT Bandwidth [5Gbps , 10Gbps]
         if self.h_tick=='16':  self.load_attribute('PS Sensor spacing','1.6 mm')
         if self.h_tick=='26':  self.load_attribute('PS Sensor spacing','2.6 mm')
         if self.h_tick=='40':  self.load_attribute('PS Sensor spacing','4.0 mm')

      if self.h_kind == 'PS Power Hybrid':             # Attributes not defined for PS-POH:
         pass                                          #   Visual inspection status [Good , Bad]
                                                       #   Electrical test status [Good , Bad]


      cdict['kind_of_part']    = self.h_kind
      cdict['unique_location'] = location 
      cdict['manufacturer']    = self.h_vend
      cdict['batch_number']    = self.h_code[1:3]
      # cdict['version']         = 'Empty'    Should we define a version string?

      if comment!=None: cdict['description'] = comment
      if inserter!=None: cdict['inserter'] = inserter
      if len(self.attributes)!=0: cdict['attributes'] = self.attributes

      #print (cdict)
      BaseUploader.__init__(self, name, cdict)

      self.mandatory += ['manufacturer','kind_of_part','unique_location']

      # Override version in configuration dictionary
      #if c_chip!=None and c_spacing!=None and c_stiffener!=None:
      #   version_str = '{}, {}, {}'.format(c_chip,c_spacing,c_stiffener)
      #   self.update_configuration('version',version_str)


   def load_attribute(self, name, value):
      """Load a component attribute. Check the allowed attributes."""
      allowed_names = self.allowed_attributes.keys()
      if name not in allowed_names:
         raise BadParameters(self.__class__.__name__,f'{name} is not an attribute of {self.h_kind}.')
      allowed_values = self.allowed_attributes[name]
      if value not in allowed_values:
         raise BadParameters(self.__class__.__name__,f'{value} is not allowed for {name} attribute.')
      self.attributes.append( (name, value) )


   def xml_builder(self,parts):
      """Process data."""
      # Gather the serial number of component
      Serial = self.name.split('_')[0]
      self.build_parts_on_xml(parts,serial=Serial)


   def dump_xml_data(self, filename=''):
      """Writes the hybrid data in the XML file for the database upload."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
      
      part_tree = etree.SubElement(root,"PARTS")

      self.xml_builder(part_tree)

      if filename == '':
         filename = '%s.xml'%self.name
      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                        xml_declaration = True,\
                                        standalone="yes").decode('UTF-8') )
      return filename




class AttachChildrenToParent(BaseUploader):
   """Class to produce the xml file to update the parent <-> child relationship
      of components. The serial number of parents are given in input, while the
      serial number of children are taken from external sources.
   """

   def __init__(self, name , cdict):
      """Constructor: it requires the instance name, the dict for configuration
         (cdict) defining the ancillary data needed for the database upload.
         Mandatory parameters in configuration are:
            kind_of_part:      the kind of part of children;
            unique_location:   the location where the children components are;
      """
      BaseUploader.__init__(self, name, cdict)

      self.mandatory += ['kind_of_part','unique_location']

   def dump_xml_data(self, filename='', associated=[], excluded=[]):
      """Writes the wafer data in the XML file for the database upload.
         Optional input parameters:
           filename     name of the xml file to be produced;
           excluded     list of parent serial IDs to be excluded from the upload; 
         Output parameter: 
           associated   list of parent serial id associated with chips."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
      
      part_tree = etree.SubElement(root,"PARTS")

      self.xml_builder(part_tree,associated,excluded)

      if filename == '':
         filename = '%s_chip_association.xml'%self.name
      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )

      return filename



class AttachCBC2FEH(AttachChildrenToParent):
   """Class to produce the xml file to update the parent <-> child relationship
      among FE Hybridis and its chips. The serial numbers of the chips attached 
      to the hybrid component are read from the output of the calibration / test
      runs.
   """

   def __init__(self, serial, kop, logs_dir, number_of_cbc, cdict):
      """Constructor: it requires the serial number of the FE Hybrid (serial), the 
         kind of part of the FE Hybrid (kop), the directory where the output from 
         calibration / test runs are located (logs_dir), the number of CBC in the 
         circuit (number_of_cbc) and the dict for configuration (cdict) defining 
         the ancillary data needed.
      """
      AttachChildrenToParent.__init__(self,serial,cdict)

      self.log_files  = search_files(logs_dir,pattern='FE0CBC[0-7].txt')
      self.parent_kop = kop #'8CBC3 Front-end Hybrid'
      self.number_of_cbc = number_of_cbc

      self.update_configuration('kind_of_part','CBC3 Readout Chip')


   def xml_builder(self,parts,*args):
      """Process data."""
      # Gather the list of parents to be excluded
      excluded = args[1]

      # Gather the serial number of parent
      Serial = self.name
      if Serial in excluded: return

      # Filter out the list of log file using the serial number of the parent
      cbc_log_to_process = [f for f in self.log_files if Serial in f]
      cbc_log_to_process.sort()
      latest_cbc_files = cbc_log_to_process[-self.number_of_cbc:]

      # If less than 8 files exit and not produce XML
      if len(latest_cbc_files)!=self.number_of_cbc:
        msg = 'Number of expected CBC calibration file is {},  but {} were found!'.\
              format(self.number_of_cbc,latest_cbc_files)
        raise BadCBCCalibration('AttachCBC2FEH',msg)

      CBCIDs = []  #contains the integer value of the CBCid of all chips

      for num, cbc_file in enumerate(latest_cbc_files):
         with open(cbc_file, 'r') as f:
            CBCid = "000000"
            for line in f:
               if 'ChipIDFuse' in line:
                  code = line.split()[4][2:]
                  if 'ChipIDFuse1' in line:  CBCid   = CBCid[:3] + code
                  elif 'ChipIDFuse2' in line:  CBCid = CBCid[:1] + code + CBCid[-2:]
                  elif 'ChipIDFuse3' in line:  CBCid = code + CBCid[-4:]

         CBCserial = int(CBCid,16)
         if CBCserial == 0:
            msg = "Found a zero CBC serial number for {} at 0x0{}".format(Serial,num+1)
            raise BadCBCCalibration('AttachCBC2FEH',msg)
         else:
            CBCIDs.append( CBCserial )

      if len(CBCIDs)!=self.number_of_cbc or len(CBCIDs) > len( set(CBCIDs) ):
         raise BadCBCCalibration('AttachCBC2FEH',\
                 'corrupted configuration for {}'.format(Serial))

      hybrid = etree.SubElement(parts, "PART", mode="auto")
      etree.SubElement(hybrid,"KIND_OF_PART").text  = self.parent_kop
      etree.SubElement(hybrid,"SERIAL_NUMBER").text = Serial
      children = etree.SubElement(hybrid,"CHILDREN")

      for num, c in enumerate(CBCIDs):
         posn = '0X0{}'.format(num+1)
         self.configuration['attributes'] = [('Chip Posn on Flex Hybrid',posn)]
         self.update_parts_on_xml(children,serial=str(c))

      try:
         args[0].append( Serial )
      except:
         pass


class RunHeader4HR(ConditionData):
   """Produces the XML file for storing the run meatadata."""

   def __init__(self, serial, run_conf):
      """Constructor: it requires the description of the run data (run_conf).

         Mandatory configuration is:
            run_name:        name of condition run measurement;
            run_type:        type of condition run measurement;
            run_number:      run number to be associated to the run type
            run_begin:       timestamp of the begin of run
         run_name and run_type + run_number are exclusive. 

         Optional parameters are:
            run_end:      timestamp of the end of run
            run_operator: operator that performed the run
            run_location: site where the run was executed
            run_comment:  description/comment on the run
            inserter:     person recording the run metadata (override the user)
      """

      name = '{}_run_metadata'.format(serial)
      ConditionData.__init__(self, name, run_conf, None)
      self.only_run = True


   def dump_xml_data(self):
      """Writes the run_header in a XML file to be uploaded in the database."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
      
      self.xml_builder(root)

      filename = '{}.xml'.format(self.name)
      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )


class ReferenceCutsT(ConditionData):
   """Produces the XML file for storing the reference cuts for accepting the Hybrids."""
   data_description  = {'POH_acc':  {  
                  'kind_of_part' : 'PS Power Hybrid',    
                     'cond_name' : 'POH Test Reference',
                    'table_name' : 'REF_TEST_POH'
                                    }
                       }
   
   def __init__(self, name, run_conf, acc_type, version='v1', inserter=None):
      """Constructor: it requires the name of the csv data file, the description of the 
         run data (run_conf), the dataset version (version) and the data inserter if the user 
         account must be overwritten. The run number will be automaticallyn increased if the
         run_number parameter will not be given. 

         Mandatory configuration is:
            run_name:        name of condition run measurement;
            run_type:        type of condition run measurement;
            run_number:      run number to be associated to the run type
            run_begin:       timestamp of the begin of run
         run_name and run_type + run_number are exclusive. 

         Optional parameters are:
            run_end:      timestamp of the end of run
            run_operator: operator that performed the run
            run_location: site where the run was executed
            run_comment:  description/comment on the run
            inserter:     person recording the run metadata (override the user)
      """

      configuration = {}
      configuration.update(self.data_description[acc_type])
      configuration.update(run_conf)
      configuration['data_version']=version
      if inserter!="":  configuration['inserter']=inserter
      
      ConditionData.__init__(self, name, configuration, None)
      self.attach_part = False


   def build_data_block(self,dataset):
      """Builds the data block."""
      etree.SubElement(dataset,"DATA_FILE_NAME").text = f'{self.name}.csv'


   def dump_xml_data(self):
      """Writes the run_header in a XML file to be uploaded in the database."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
      
      self.xml_builder(root)

      filename = '{}.xml'.format(self.name)
      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )



class HybridResultsT(Hybrid,ConditionData):
   """Produces the XML file for storing the Hybrid result data."""
   data_description  = {('PS Front-end Hybrid','Electrical'):  {  
                                 'kind_of_part' : 'PS Front-end Hybrid',     
                                    'cond_name' : 'Tracker PS FEH Test Result',
                                   'table_name' : 'TEST_PSFEH',
                           'DBvar_vs_TxtHeader' : {'QUALIFICATION'     : '',
                                                   'FAILURE_CAUSE'     : '',
                                                   'KNOWN_PROBLEMS'    : '',
                                                   'COMPUTER'          : '',
                                                   'LOCAL_DATA_PATH'   : '',
                                                   'START_TIME'        : '',
                                                   'END_TIME'          : '',
                                                   'TEST_CARD'         : '',
                                                   'BACKPLANE_POS'     : '',
                                                   'FC7_MACADDR'       : '', 
                                                   'SOFTWARE_VER'      : '',
                                                   'E_MEASUREMENT'     : '',
                                                   'OPENS'             : '',
                                                   'OPEN_CH'           : '',
                                                   'SHORTS'            : '',
                                                   'SHORTED_CH'        : '',
                                                   'PEDESTAL_AVG'      : '',
                                                   'PEDESTAL_RMS'      : '',
                                                   'NOISE_AVG'         : '',
                                                   'NOISE_RMS'         : '',
                                                   'CIC_INTEST'        : '',
                                                   'CIC_INBADLINES'    : '',
                                                   'CIC_OUTTEST'       : '',
                                                   'CIC_OUTBADLINES'   : '',
                                                   'SSA_OUTTEST'       : '',
                                                   'SSA_BAD_STUBLINES' : '',
                                                   'SSA_BAD_L1LINES'   : '',
                                                   'SSA_BAD_LATERAL'   : ''},
                                    'mandatory' : ['QUALIFICATION','TEST_CARD','BACKPLANE_POS',
                                                   'FC7_MACADDR','SOFTWARE_VER']
                                                   }
                        }   

   def __init__(self, serial, test_type, data_version='v1', inserter=None):
      """Constructor: it requires the serial number and the type of test (test_type). It also
         accepts the data set version (set to 'v1' as default) and the inserter name which is
         not used by default.

         Mandatory template configuration is:
            run_name:        name of condition run measurement;
            run_type:        type of condition run measurement;
            run_number:      run number to be associated to the run type
            run_begin:       timestamp of the begin of run
            kind_of_part:    the type of component whose data belong to;
            serial:          identifier of the component whose data belong to;
            barcode:         identifier of the component whose data belong to;
            name_label:      identifier of the component whose data belong to;
            cond_name:       kind_of_condition name corresponding to these data;
            table_name:      condition data table where data have to be recorded;
            data_version:    data set version;
            DBV_vs_Theader:  describe how to associate data to the DB variables;
         run_name and run_type + run_number are exclusive. Serial, barcode and
         name_label are mutual exclusive.

         Optional template parameters are:
            run_end:      timestamp of the end of run
            run_operator: operator that performed the run
            run_location: site where the run was executed
            run_comment:  description/comment on the run
            data_comment: description/comment on the dataset
            inserter:     person recording the data (override the CERN user name)
      """

      Hybrid.__init__(self,serial)

      self.h_test = test_type    # stores the type of test

      name = '{}_results'.format(serial)

      configuration = {}
      data_description=self.data_description[(self.h_kind,self.h_test)]
      configuration.update(deepcopy(data_description))
      configuration['serial'] = serial
      configuration['data_version']=data_version
      

      ConditionData.__init__(self, name, configuration, None)


   def build_data_block(self,dataset):
      """Builds the data block."""
      data_description = self.retrieve('DBvar_vs_TxtHeader')
      data = etree.SubElement(dataset,"DATA")
      tags = data_description.keys()

      # Check for mandatory tags
      for el in self.retrieve("mandatory"):
         if data_description[el]=='':
            raise MissingParameter(self.__class__.__name__,el)

      for el in data_description.keys():
         etree.SubElement(data,f"{el}").text = data_description[el]


   def load_run_metadata(self, run_type=None, run_number=None, run_begin=None, run_end=None, 
                               run_operator=None, run_location=None, run_comment=None ):
      """Loads the run metadata."""
      if run_type!=None: self.update_configuration('run_type',run_type)
      if run_number!=None: self.update_configuration('run_number',run_number)
      if run_begin!=None: self.update_configuration('run_begin',run_begin.strftime('%Y-%m-%d %H:%M:%S'))
      if run_end!=None: self.update_configuration('run_end',run_end.strftime('%Y-%m-%d %H:%M:%S'))
      if run_operator!=None: self.update_configuration('run_operator',run_operator)
      if run_location!=None: self.update_configuration('run_location',run_location)
      if run_comment!=None: self.update_configuration('run_comment',run_comment)


   def load_results(self,hybrid_status=None, test_result_cause=None, test_known_problems=None,
                    electrical_measurements=None, noise_measurements=None,
                    cic_in_test=None, cic_out_test=None, ssa_out_test=None, 
                    opens=None, shorts=None):
      """Loads the data block."""
      if hybrid_status!=None: self.process_qualification(hybrid_status)
      if test_result_cause!=None: self.update_data_block('FAILURE_CAUSE',test_result_cause)
      if test_known_problems!=None: self.update_data_block('KNOWN_PROBLEMS',test_known_problems)

      if electrical_measurements!=None: self.update_data_block('E_MEASUREMENT',\
                                           json.dumps(electrical_measurements))
      if noise_measurements!=None:
         self.update_data_block('PEDESTAL_AVG',str(noise_measurements['pedestal_avg']))
         self.update_data_block('PEDESTAL_RMS',str(noise_measurements['pedestal_rms']))
         self.update_data_block('NOISE_AVG',str(noise_measurements['noise_avg']))
         self.update_data_block('NOISE_RMS',str(noise_measurements['noise_rms']))

      if cic_in_test!=None:  self.process_cic_in_test(cic_in_test)
      if cic_out_test!=None:  self.process_cic_out_test(cic_out_test)
      if ssa_out_test!=None: self.process_ssa_out_test(ssa_out_test)
      if opens!=None:  self.process_opens(opens)
      if shorts!= None:  self.process_shorts(shorts)


   def load_test_spec(self, computer=None, data_path=None, start_time=None,
                      stop_time=None, test_card=None, backplane_pos=None,
                      fc7_macaddr=None, software_ver=None):
      """Loads the test specification."""
      if computer!=None: self.update_data_block('COMPUTER',computer) 
      if data_path!=None: self.update_data_block('LOCAL_DATA_PATH',data_path)
      if start_time!=None: self.update_data_block('START_TIME',start_time.strftime('%Y-%m-%d %H:%M:%S'))
      if stop_time!=None: self.update_data_block('END_TIME',stop_time.strftime('%Y-%m-%d %H:%M:%S'))
      if test_card!=None: self.update_data_block('TEST_CARD',test_card)
      if backplane_pos!=None: self.update_data_block('BACKPLANE_POS',json.dumps(backplane_pos)) 
      if fc7_macaddr!=None: self.update_data_block('FC7_MACADDR',fc7_macaddr)
      if software_ver!=None: self.update_data_block('SOFTWARE_VER',software_ver)
      

   def process_qualification(self,qualification):
      """Check QUALIFICATION format."""
      if qualification!='Good' and qualification!='Bad' and qualification!='Needs retest':
         raise BadInputParameter('HybridResultT.process_qualification','QUALIFICATION',qualification)
      self.update_data_block('QUALIFICATION',qualification)


   def process_opens(self,opens):
      """Decode the opens test result and encode it as JSON format."""
      self.update_data_block('OPENS',str(opens['total_opens']))
      open_channels = { 'SSA'+k.split('_')[1]:v for (k,v) in opens.items() if k!='total_opens'}
      if len(open_channels)!=0:
         self.update_data_block('OPEN_CH',json.dumps(open_channels))


   def process_shorts(self,shorts):
      """Decode the opens test result and encode it as JSON format."""
      self.update_data_block('SHORTS',str(shorts['total_shorts']))
      shorted_channels = { 'SSA'+k.split('_')[1]:v for (k,v) in shorts.items() if k!='total_shorts'}
      if len(shorted_channels)!=0:
         self.update_data_block('SHORTED_CH',json.dumps(shorted_channels))


   def process_cic_in_test(self,cic_in_test):
      """Decode the CIC IN test results and encode them as JSON format."""
      status = cic_in_test['STATUS'].upper()
      if status!='OK' and status!='NOT OK': 
         raise BadInputParameter('HybridResultT.process_cic_in_test','STATUS',status)
      self.update_data_block('CIC_INTEST',status)
      try:
         self.update_data_block('CIC_INBADLINES',json.dumps(cic_in_test['bad_lines']))
      except:  pass

   def process_cic_out_test(self,cic_out_test):
      """Decode the CIC OUT test results and encode them as JSON format."""
      status = cic_out_test['STATUS'].upper()
      if status!='OK' and status!='NOT OK': 
         raise BadInputParameter('HybridResultT.process_cic_out_test','STATUS',status)
      self.update_data_block('CIC_OUTTEST',status)
      try:
         self.update_data_block('CIC_OUTBADLINES',json.dumps(cic_out_test['bad_lines']))
      except:  pass

   def process_ssa_out_test(self,ssa_out_test):
      """Decode the SSA test results and encode as JSON format."""
      status = ssa_out_test['STATUS'].upper()
      if status!='OK' and status!='NOT OK': 
         raise BadInputParameter('HybridResultT.process_ssa_out_test','STATUS',status)

      self.update_data_block('SSA_OUTTEST',status)
      ssa_with_badlines = sorted( [k for k in ssa_out_test.keys() if 'SSA' in k] )
      
      ssa_bad_stublines = {}
      for ssa in ssa_with_badlines:  
         if 'stub_lines' in ssa_out_test[ssa].keys():
            lines = ssa_out_test[ssa]['stub_lines']
            ssa_bad_stublines[ssa] = [ int(x) for x in lines ]
      if len(ssa_bad_stublines)!=0:
         self.update_data_block('SSA_BAD_STUBLINES',json.dumps(ssa_bad_stublines))

      ssa_bad_l1lines = [ssa for ssa in ssa_with_badlines if 'L1' in ssa_out_test[ssa].keys()]
      if len(ssa_bad_l1lines)!=0:
         self.update_data_block('SSA_BAD_L1LINES', json.dumps(ssa_bad_l1lines))

      ssa_bad_laterals = [(f'SSA{c[0]}',f'SSA{c[1]}') for c in ssa_out_test['ssa_lateral_bad_lines']]     
      if len(ssa_bad_laterals)!=0:
         self.update_data_block('SSA_BAD_LATERAL', json.dumps(ssa_bad_laterals))


   def load_root_file(self,data_path):
      """Load the root file."""
      if (self.h_kind=='PS Front-end Hybrid'):   # Merge the two result files.
         import subprocess, os
         
         files  = search_files(data_path,"*.root")
         target = os.path.join(os.getcwd(),f'{self.serial}_results.root')
         cmd = f'cd ../Ph2_ACF; source ./setup.sh; hadd -f {target} {files[0]} {files[1]}' 
         p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE,
                              bufsize=0, close_fds=True, encoding='UTF-8', shell=True)

         out,err = p.communicate()
         print (err)
         if p.returncode!=0:
            raise BadExecution(cmd,err)
      else:
         from shutil import copyfile
         source = os.path.join(data_path,'Hybrid.root')
         target = os.path.join(os.getcwd(),f'{self.serial}_results.root')
         copyfile(source,target)
      
      data_block = self.retrieve('DBvar_vs_TxtHeader')
      filename   = f'{self.serial}_results.root'
      filetime   = os.path.getmtime(filename)
      from datetime import datetime
      dt = datetime.fromtimestamp(filetime)
      data_block['ROOT_FILE'] = filename
      data_block['ROOT_DATE'] = dt.strftime('%Y-%m-%d %H:%M:%S')


   def update_data_block(self,name,value):
      """Update the data block content. Check the consistency of the name tag."""
      data_block = self.retrieve('DBvar_vs_TxtHeader')
      data_block_tags = data_block.keys()
      if name not in data_block_tags:
         raise BadParameters(self.__class__.__name__,f'{name} is not a tag for {self.h_kind} {self.h_test}.')
      data_block[name]=value
      
      
   def dump_xml_data(self, filename=''):
      """Dump the sensor condition data in the XML file for the database upload.
         It requires in input the name of the XML file to be produced."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)

      self.xml_builder(root)

      if filename == '':
         filename = '{}.xml'.format(self.name)

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )
      
      from zipfile import ZipFile
      import os
      zipfilename = f'{self.serial}_step2.zip'
      to_be_zipped = [f'{self.serial}_results.xml',\
                     f'{self.serial}_results.root']
      with ZipFile(zipfilename, 'w') as (zip):
         for f in to_be_zipped: zip.write(f)
         for f in to_be_zipped: os.remove(f)
      
      return zipfilename
