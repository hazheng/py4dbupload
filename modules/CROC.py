#!/usr/bin/python
# $Id$
# Created by Alessandro Di Mattia <dimattia@cern.ch>, 12-Oct-2021

# This module contains the classes that produce the XML file for uploading
# the CROC Wafer, Chip and their Condition data.


from Exceptions   import MissingData, BadStripInconsistency, BadMeasurement,\
                         NotRecognized
from BaseUploader import BaseUploader,ComponentType, ConditionData
#from DataReader   import scale2, consistency_check
from lxml         import etree
from time         import gmtime, strftime
from enum         import Enum
from datetime     import datetime

import os,sys


class CROCwafer(BaseUploader):
   """Class to produce the xml file for the DBloader. It handles the CROC Wafer
      component data read from the csv file."""
   db_check = True

   def __init__(self, configuration, database, batch_nn, batch_id, wafer_id):
      """Constructor: it requires the dict for configuration (configuration)
         defining the user data for the upload the access to the database, the
         wafer batch number (batch_nn), the batch ID (batch_id) and the wafer
         ID (wafer_id).
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            attributes:      the additional attributes for the Wafer;
            kind_of_part:    the part to be inserted in the database;
            unique_location: the location where the component is located;
            version:         the version tag used to define the kind of part;
      """

      import copy
      self.type = ComponentType.CROCwafer
      self.database = database
      #self.batch_nn = batch_nn
      cdict = copy.deepcopy(configuration)


      # Use TMSC as the default for Manufacturer and CERN for Location
      if 'manufacturer' not in cdict.keys():  cdict['manufacturer'] = 'TSMC'
      if 'unique_location' not in cdict.keys():cdict['unique_location'] = 'CERN'

      # The class name is the name_label of the Wafer written in the txt file
      name = '{}_{}'.format(batch_nn,wafer_id)

      # Set kind_of_part according to the version tag and batch number
      ver_name = cdict['version']
      kop_name = 'CROC Wafer' if '1.0' not in ver_name else 'CROC Proto Wafer'
      cdict['kind_of_part'] = kop_name
      cdict['batch_number'] = batch_id

      # Set the production date and the extended data
      #objDate = datetime.strptime(dreader.getDataFromTag('#Date'),'%d-%b-%y')
      #cdict['product_date'] = datetime.strftime(objDate,'%Y-%m-%d')
      #cdict['ingot_number'] = dreader.getDataFromTag('#Ingot No.')

      #Check that Polysilicon Bias resistance are float
      #try:
        # value = float( dreader.getDataFromTag('#Polysilicon Bias Resistance Upper [Mohm]') )
        # cdict['psbiasres_up'] = str(value)
      #except:  pass
      #try:
        # value = float( dreader.getDataFromTag('#Polysilicon Bias Resistance Lower [Mohm]') )
        # cdict['psbiasres_lo'] = str(value)
      #except:  pass
      #try:
        # value = float( dreader.getDataFromTag('#Polysilicon Bias Resistance Average [Mohm]') )
        # cdict['psbiasres_av'] = str(value)
      #except:  pass

      BaseUploader.__init__(self, name, cdict)
      self.mandatory += ['kind_of_part','unique_location','manufacturer',\
                          'attributes','version']

      #initialize the envelope_ids
      #self.envelope_ids = set()
      #if envelope_ids!=None:
    #     data = envelope_ids.getDataAsCWiseVector()
    #     self.envelope_ids = sorted( { n[:13] for n in data[0] if n!='' } )

   def name_label(self):
      """Returns the name label of the component."""
      return self.name

   def batch_nn(self,name_label=None):
      """Return the batch id from the name label."""
      if name_label==None:  name_label = self.name
      return name_label.split('_')[0]

   def wafer_id(self,name_label=None):
      """Return the wafer_id from the name label."""
      if name_label==None:  name_label = self.name
      return name_label.split('_')[1]

   def match(self,name_label):
      """Return True if the inout name_label matches the class name."""
      if self.batch_nn()    == self.batch_nn(name_label) and\
         self.wafer_id()    == self.wafer_id(name_label):
        return True
      return False

   def kind_of_part(self,version=None):
      """Return the kind_of_part to be associated to this barcode."""
      if version==None:  version = self.retrieve('version')
      kop_name = 'CROC Wafer' if '1.0' not in version else 'CROC Proto Wafer'
      return kop_name

   def dump_xml_data(self, filename=''):
      """Writes the wafer components in the XML file for the database upload.
         It requires in input the name of the XML file to be produced."""
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
         
      parts = etree.SubElement(root,"PARTS")

      self.xml_builder(parts)

      if filename == '':
         filename = '{}_wafer.xml'.format(self.name_label())

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )


   def xml_builder(self, parts, *args):
      """Processes data."""
      #batchN  = self.batch_number()
      #waferN  = self.wafer_number()
      #waferT  = self.wafer_type()
      #label   = '{}_{}_{}'.format(batchN,waferN,waferT)
      #bcode   = '{}_{}_{}'.format(batchN,waferN,waferT)

      #for eid in self.envelope_ids:
    #      if self.match(eid):   bcode = eid

      #ex= [ ( 'INGOT_NR', self.retrieve('ingot_number') ) ]
      #if self.retrieve('psbiasres_up'):
        # ex.append( ( 'PBIASRES_UPPER_MOHM', self.retrieve('psbiasres_up')) )
      #if self.retrieve('psbiasres_lo'):
        # ex.append( ( 'PBIASRES_LOWER_MOHM', self.retrieve('psbiasres_lo')) )
      #if self.retrieve('psbiasres_av'):
        # ex.append( ( 'PBIASRES_AVERAGE_MOHM', self.retrieve('psbiasres_av')) )

      registered = False
      if self.db_check:
         registered = self.database.component_in_db(self.name_label(),'name_label')

      if not registered:
         self.build_parts_on_xml(parts,name_label=self.name_label())
         #self.build_parts_on_xml(parts,barcode=bcode,name_label=label,extended_data=ex)


class CROCchip(BaseUploader):
   """Class to produce the xml file for the DBloader. It handles the CROC chip
      component data."""
   db_check = False

   def __init__(self, configuration, wafer_namelabel, wafer_nn, chip_id):
      """Constructor: it requires the dict for configuration (configuration)
         defining the ancillary data needed for the database upload the wafer
         name_label (wafer_namelabel), the wafer name (wafer_nn) and the chip
         ID (chip_id).
         Mandatory parameters in configuration:
            manufacturer:    the manufacturer of the Wafer;
            attributes:      the additional attributes for the Wafer;
            kind_of_part:    the part to be inserted in the database;
            unique_location: the location where all the components are;
            version:         the version tag used to define the kind of part;
      """

      import copy
      self.type = ComponentType.CROCchip
      self.waferN = wafer_nn
      cdict = copy.deepcopy(configuration)

      # Use Hamamatsu as the default for Manufacturer and Location
      if 'manufacturer' not in cdict.keys():  cdict['manufacturer'] = 'TSMC'
      if 'unique_location' not in cdict.keys():cdict['unique_location'] = 'CERN'

      # The class name is the name_label of the CROC chip
      name = '{}_{}'.format(wafer_namelabel,chip_id)

      # Set kind_of_part according to the version tag
      ver_name = cdict['version']
      kop_name = 'CROC Chip' if '1.0' not in ver_name else 'CROC Proto Chip'
      cdict['kind_of_part'] = kop_name


      BaseUploader.__init__(self, name, cdict)
      self.mandatory += ['kind_of_part','unique_location','manufacturer',\
                         'attributes','version']

      attributes = self.retrieve('attributes')
      from ast import literal_eval
      row_on_wafer = 'Row {}'.format(literal_eval('0x'+chip_id[0]))
      col_on_wafer = 'Col {}'.format(literal_eval('0x'+chip_id[1]))
      attributes.append( ('Chip Row on Wafer', row_on_wafer) )
      attributes.append( ('Chip Column on Wafer', col_on_wafer) )
      self.update_configuration('attributes',attributes)
      #print (self.name, self.retrieve('attributes'))



   def name_label(self):
      """Returns the name label of the component."""
      return self.name

   def barcode(self):
      """Returns the barcode of the component."""
      return '{}{}{}'.format(self.batch_nn(),self.wafer_nn(),self.chip_id())

   def batch_nn(self,name_label=None):
      """Return the batch id from the name label."""
      if name_label==None:  name_label = self.name
      return name_label.split('_')[0]

   def wafer_id(self,name_label=None):
      """Return the wafer_id from the name label."""
      if name_label==None:  name_label = self.name
      return name_label.split('_')[1]

   def chip_id(self,name_label=None):
      """Return the chip_id from the name label."""
      if name_label==None:  name_label = self.name
      return name_label.split('_')[2]

   def wafer_nn(self):
      """Return the wafer_nn given at init time."""
      return self.waferN

   def match(self,name_label):
      """Return True if the inout name_label matches the class name."""
      if self.batch_nn()    == self.batch_nn(name_label) and\
         self.wafer_id()    == self.wafer_id(name_label) and\
         self.chip_id()     == self.chip_id(name_label):
        return True
      return False

   def kind_of_part(self,version=None):
      """Return the kind_of_part to be associated to this barcode."""
      if version==None:  version = self.retrieve('version')
      kop_name = 'CROC Chip' if '1.0' not in ver_name else 'CROC Proto Chip'
      return kop_name

   def dump_xml_data(self,filename='',wafers=None):
      """Writes the sensor component in the XML file for the database upload.
         It requires the name of the XML file to be produced. A CROCwafer
         class can also be input: in this case a parent-child relationship
         with the Wafer components is set (the wafer components must have
         been already entered in the database).
      """
      inserter  = self.retrieve('inserter')
      root      = etree.Element("ROOT")
      if inserter is not None:
         root.set('operator_name',inserter)
         
      parts = etree.SubElement(root_tree,"PARTS")

      self.xml_builder(parts,wafers)

      if filename == '':
         filename = '%s_sensors.xml'%self.name

      with open(filename, "w") as f:
         f.write( etree.tostring(root, pretty_print = True, \
                                       xml_declaration = True,\
                                       standalone="yes").decode('UTF-8') )

   def xml_builder(self, parts, *args):
      """Processes data in the reader."""
      wafers = args[0]

      children = parts
      if wafers is not None:
         for wafer in wafers:
            if wafer.match(self.name_label()):
               wafer_kop  = wafer.kind_of_part()
               wafer_part = etree.SubElement(parts, "PART", mode="auto")
               etree.SubElement(wafer_part,"KIND_OF_PART").text  = wafer_kop
               etree.SubElement(wafer_part,"NAME_LABEL").text = wafer.name_label()
               children = etree.SubElement(wafer_part,"CHILDREN")

      registered = False
      if self.db_check:
         registered = self.database.component_in_db(self.name_label(),'name_label')

      if not registered:
        self.build_parts_on_xml(children,name_label=self.name_label(),barcode=self.barcode())
